package com.ooad.onlinestore.domain;

import com.ooad.onlinestore.domain.model.dto.OrderDto;
import com.ooad.onlinestore.domain.model.dto.PaymentDto;

public interface PurchaseManager {
	PaymentDto processOrder(OrderDto order);
}
