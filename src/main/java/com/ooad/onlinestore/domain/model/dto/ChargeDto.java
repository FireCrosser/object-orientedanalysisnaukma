package com.ooad.onlinestore.domain.model.dto;

public interface ChargeDto {
	int getTotalCost();
}
