package com.ooad.onlinestore.payment.general;

import com.ooad.onlinestore.domain.model.dto.ChargeDto;
import com.ooad.onlinestore.domain.model.dto.InvoiceDto;

public interface PaymentRequest {
	
	void setInvoice(InvoiceDto invoice);
	InvoiceDto getInvoice();
	void setCharge(ChargeDto charge);
	ChargeDto getCharge();

}
