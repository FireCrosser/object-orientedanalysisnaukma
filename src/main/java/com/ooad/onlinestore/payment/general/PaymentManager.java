package com.ooad.onlinestore.payment.general;

public interface PaymentManager {
	PaymentResponse processPayment(PaymentRequest request);
}
